const mysql = require('mysql2/promise')

/**
 CREATE TABLE logs (
    id INT AUTO_INCREMENT PRIMARY KEY,
    log_text TEXT,
    create_time int
);
 */
let connection
async function getConnection(){
    if (!connection){
        connection = await mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '123456', //fZ1akp+EOIFW
            database: 'jiushi',
          });
    }
    return connection
}

async function run(sql, values=[]){
// 创建一个数据库连接

  
  // 简单查询
  try {
    const connection = await getConnection()
    const [results, fields] = await connection.query(
        sql, values
    );
  
    console.log(results); // 结果集
    console.log(fields); // 额外的元数据（如果有的话）
  } catch (err) {
    console.log(err);
  }
}

module.exports = run